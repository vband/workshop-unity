﻿using UnityEngine;

namespace WorkshopCode.MouseRotation
{
    public class MouseRotationBehaviour : MonoBehaviour
    {
        private Vector2 _lastMousePosition;

        private float _rotationX, _rotationY;

        private void Start()
        {
            _lastMousePosition = Input.mousePosition;

            Cursor.visible = false;
        }

        private void Update()
        {
            Vector2 currentMousePosition = Input.mousePosition;
            Vector2 mousePositionDelta = currentMousePosition - _lastMousePosition;

            _rotationX += mousePositionDelta.y;
            _rotationY += mousePositionDelta.x;

            _rotationX = Mathf.Clamp(_rotationX, -45f, 45f);
            transform.eulerAngles = new Vector3(-_rotationX, _rotationY, 0);
            
            _lastMousePosition = currentMousePosition;
        }
    }
}