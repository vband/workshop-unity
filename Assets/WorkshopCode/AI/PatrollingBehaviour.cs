﻿using System;
using UnityEngine;
using UnityEngine.AI;
using Random = UnityEngine.Random;

namespace WorkshopCode.AI
{
    public class PatrollingBehaviour : MonoBehaviour
    {
        [SerializeField] private NavMeshAgent _agent;

        [SerializeField] private float _changeDestinationInterval = 5f;

        private float _currentPatrolTime;

        private void Start()
        {
            StartPatrol();
        }

        private void Update()
        {
            if (_currentPatrolTime >= _changeDestinationInterval)
            {
                StartPatrol();
            }

            _currentPatrolTime += Time.deltaTime;
        }

        private void StartPatrol()
        {
            _agent.SetDestination(GetRandomDestination());
            _currentPatrolTime = 0;
        }

        private Vector3 GetRandomDestination()
        {
            NavMesh.SamplePosition(
                _agent.transform.position + new Vector3(Random.Range(-20f, 20f), 0, Random.Range(-20f, 20f)),
                out var hit, Single.PositiveInfinity, _agent.areaMask);

            return hit.position;
        }
    }
}