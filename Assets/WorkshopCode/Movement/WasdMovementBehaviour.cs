using UnityEngine;

namespace WorkshopCode.Movement
{
    public class WasdMovementBehaviour : MonoBehaviour
    {
        [SerializeField] private Rigidbody _rigidbody;

        [SerializeField] private Transform _forwardTransform;

        [SerializeField] private float _movementSpeed = 5f;

        private void Update()
        {
            bool isMovingAlongZAxis = false;
            bool isMovingAlongXAxis = false;
            
            if (Input.GetKey(KeyCode.W))
            {
                isMovingAlongZAxis = true;
                SetVelocity(ProjectOnPlaneAndNormalize(_forwardTransform.forward));
            }
            else if (Input.GetKey(KeyCode.S))
            {
                isMovingAlongZAxis = true;
                SetVelocity(ProjectOnPlaneAndNormalize(-_forwardTransform.forward));
            }
            if (Input.GetKey(KeyCode.A))
            {
                isMovingAlongXAxis = true;
                
                if (isMovingAlongZAxis)
                    AddVelocity(ProjectOnPlaneAndNormalize(-_forwardTransform.right));
                else
                    SetVelocity(ProjectOnPlaneAndNormalize(-_forwardTransform.right));
            }
            else if (Input.GetKey(KeyCode.D))
            {
                isMovingAlongXAxis = true;
                
                if (isMovingAlongZAxis)
                    AddVelocity(ProjectOnPlaneAndNormalize(_forwardTransform.right));
                else
                    SetVelocity(ProjectOnPlaneAndNormalize(_forwardTransform.right));
            }

            if (!isMovingAlongZAxis && !isMovingAlongXAxis)
                StopMovement();
        }

        private Vector3 ProjectOnPlaneAndNormalize(Vector3 direction)
        {
            return Vector3.ProjectOnPlane(direction, Vector3.up).normalized;
        }

        private void AddVelocity(Vector3 direction)
        {
            _rigidbody.velocity += direction * _movementSpeed;
        }

        private void StopMovement()
        {
            _rigidbody.velocity = new Vector3(0, 0, 0);
        }

        private void SetVelocity(Vector3 direction)
        {
            _rigidbody.velocity = direction * _movementSpeed;
        }
    }
}
