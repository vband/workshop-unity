﻿using UnityEngine;

namespace WorkshopCode.Shooting
{
    public class ShooterBehaviour : MonoBehaviour
    {
        [SerializeField] private Transform _camera;
        
        private void Update()
        {
            if (Input.GetMouseButtonDown(0))
            {
                if (Physics.Raycast(_camera.position, _camera.forward, out var hit))
                {
                    var shootable = hit.collider.GetComponent<ShootableBehaviour>();

                    if (shootable != null)
                    {
                        shootable.OnShot.Invoke();
                    }
                }
            }
        }
    }
}