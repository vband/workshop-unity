﻿using UnityEngine;
using UnityEngine.Events;

namespace WorkshopCode.Shooting
{
    public class ShootableBehaviour : MonoBehaviour
    {
        public UnityEvent OnShot;
    }
}